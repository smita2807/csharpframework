﻿using Assignments.Utilities;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace Assignments.Pages
{
    public class BuildingAutomationPage : Base
    {
        public IReadOnlyCollection<IWebElement> BreadCrumbs => CommonUtils.FindElements(_driver,By.XPath("//div[@class='breadcrumbs-block']/div/div/ul"));
        public IWebElement LnkBuildAutoArchive => CommonUtils.FindElement(_driver, By.XPath("//div[@class='navigation-block']//*[@class='sub-nav']/li/a"));
        public IReadOnlyCollection<IWebElement> ENewsletter => CommonUtils.FindElements(_driver, By.XPath("//*[@id='content']//*[@class='company-section add']/ul"));
    }
}
