﻿using Assignments.Utilities;
using OpenQA.Selenium;

namespace Assignments.Pages
{
    public class LoginPage
    {
        private IWebDriver _driver;

        public IWebElement FormLogin => CommonUtils.FindElement(_driver, By.Id("login-form"));
        public IWebElement TxtEmailAddress => CommonUtils.FindElement(_driver, By.XPath("//*[@id='field_253' and @name='auth_username_1_253']"));
        public IWebElement TxtPassword => CommonUtils.FindElement(_driver, By.XPath("//*[@id='field_254' and @name='auth_password_12_254']"));
        public IWebElement BtnSubmit => CommonUtils.FindElement(_driver, By.XPath("//*[@id='form_login']//*[@type='submit']"));

        public void PerformLogin(string emailAddress, string password)
        {
            TxtEmailAddress.SendKeys(emailAddress);
            TxtPassword.SendKeys(password);
            BtnSubmit.Click();
        }
    }
}
