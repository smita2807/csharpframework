﻿using Assignments.Utilities;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace Assignments.Pages
{
    public class SalarySurveyResultsPage:Base
    {
        public IWebElement LnkSalarySurveyResults => CommonUtils.FindElement(_driver, By.PartialLinkText("2018 SALARY SURVEY RESULTS"));
        public IWebElement LnkPastSalarySurveyResults => CommonUtils.FindElement(_driver, By.PartialLinkText("Past Salary Survey Results"));
        public IWebElement LnkSalarySurveyResults2015 => CommonUtils.FindElement(_driver, By.PartialLinkText("Salary Survey Results 2015"));
        public IWebElement H1SalarySurveyResults2015 => CommonUtils.FindElement(_driver, By.XPath("//div[@class='block']//h1[contains(text(),'Salary Survey Results 2015')]"));

        //Average Salary by Region of the World Table
        public IWebElement DivAverageSalaryByRegionWorld => CommonUtils.FindElement(_driver, By.XPath("//div[@class='heading-box']//h2[contains(text(),'Average Salary by Region of the World')]"));        
        public IWebElement TblRegionOfTheWorld => CommonUtils.FindElement(_driver, By.XPath(".//div//table[2]"));
        public IReadOnlyCollection<IWebElement> TblRowsRegionOfWorld => _driver.FindElements(By.XPath(".//div//table[2]//tbody//tr//td[1]"));
        public IReadOnlyCollection<IWebElement> TblAverageSalaryByRegionWorldRows => _driver.FindElements(By.XPath(".//div//table[2]//tbody//tr//td[2]"));
        public IReadOnlyCollection<IWebElement> TblRowsRegionWorldPercentRespondents => _driver.FindElements(By.XPath(".//div//table[2]//tbody//tr//td[3]"));

        //Average Salary by Region of the United States
        public IWebElement DivAverageSalaryByRegionUS => CommonUtils.FindElement(_driver, By.XPath("//div[@class='heading-box']//h2[contains(text(),'Average Salary by Region of the United States')]"));
        public IWebElement TblRegionOfTheUS => CommonUtils.FindElement(_driver, By.XPath(".//div//table[3]"));
        public IReadOnlyCollection<IWebElement> TblRowsRegionOfUS => _driver.FindElements(By.XPath(".//div//table[3]//tbody//tr//td[1]"));
        public IReadOnlyCollection<IWebElement> TblAverageSalaryByRegionUSRows => _driver.FindElements(By.XPath(".//div//table[3]//tbody//tr//td[2]"));
        public IReadOnlyCollection<IWebElement> TblRowsRegionUSPercentRespondents => _driver.FindElements(By.XPath(".//div//table[3]//tbody//tr//td[3]"));

    }
}
