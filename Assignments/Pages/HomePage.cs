﻿using Assignments.Utilities;
using OpenQA.Selenium;

namespace Assignments.Pages
{
    public class HomePage:Base
    {
        public IWebElement LnkHome => _driver.FindElement(By.ClassName("Home"));
        public IWebElement LnkIndustries => _driver.FindElement(By.XPath("//div[@id='wrapper']//descendant::a//*[contains(text(), 'Industries')]"));
        public IWebElement LnkJobCentre => _driver.FindElement(By.XPath("//*[@id='nav']//*[contains(text(),'Job Center')]"));
        public IWebElement SubLnkSalarySuveyResults => _driver.FindElement(By.XPath("//*[@id='nav']//*[contains(text(),'2018 Salary Survey Results')]"));
        public IWebElement LinkProducts => _driver.FindElement(By.XPath("//div[@id='header']/div/div/ul/*[3]"));
        public IWebElement CloseLink => _driver.FindElement(By.LinkText("Click to Close"));
    }
}
