﻿using Assignments.Utilities;
using OpenQA.Selenium;

namespace Assignments.Pages
{
    public class IndustriesPage:Base
    {
        public IWebElement LnkSubMenuBuildingAutomation => CommonUtils.FindElement(_driver, By.XPath("//div[@id='wrapper']//*[@class='drop']//*[contains(text(),'Building Automation')]"));
        public IWebElement TitlesOfPapers => CommonUtils.FindElement(_driver, By.XPath("//*[@id='content']/div/div[4]"));
    }
}

