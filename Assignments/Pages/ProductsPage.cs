﻿using Assignments.Utilities;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignments.Pages
{
    public class ProductsPage:Base
    {
        public IWebElement TitleProductSearch => CommonUtils.FindElement(_driver, By.XPath("//div[@id='content']//*[@class='block']"));
        public IWebElement TxtByKeyword => CommonUtils.FindElement(_driver, By.Id("field_707"));
        public IWebElement BtnSearchNow => CommonUtils.FindElement(_driver, By.XPath("//div[@id='tab-filter']//*[@class='search-button']/a"));
        public IReadOnlyCollection<IWebElement> SearchResults => _driver.FindElements(By.XPath("//div[@id='pagination-container']//*[@class='text-holder']"));
        public IWebElement BtnOpenOrCloseSearch => CommonUtils.FindElement(_driver, By.XPath("//div[@id='content']//*[@class='toggle-search']"));
        public IReadOnlyCollection<IWebElement> SearchInfoCategory => _driver.FindElements(By.XPath("//div[@id='pagination-container']//*[@class='posted-info']"));
        public IWebElement SearchProductLinks => CommonUtils.FindElement(_driver, By.XPath("//div[@id='pagination-container']//*[@class='text-holder']/h2/a"));
        public IWebElement ProductDetails => CommonUtils.FindElement(_driver, By.XPath("//div[@id='content']//*[@class='info-block']//p"));
        //public IWebElement ComboCategory => CommonUtils.FindElement(_driver, By.XPath("//div[@id='tab-filter']//*[@class='select2-selection__rendered']"));
        public IWebElement ComboCategory => CommonUtils.FindElement(_driver, By.XPath("//span[@class='select2-selection select2-selection--multiple']"));
    }
}
