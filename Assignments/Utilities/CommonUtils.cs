﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Assignments.Utilities
{
   public class CommonUtils : Base
    {
        public string ElementName { get; set; } = "";
        /// <summary>
        /// Hover over an Element
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="element"></param>
        public static void Hover(IWebDriver driver, IWebElement element)
        {
            Actions action = new Actions(driver);
            action.MoveToElement(element).Perform();
        }

        /// <summary>
        /// Hover and Click on an element
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="elementToHover"></param>
        /// <param name="elementToClick"></param>
        public static void HoverAndClick(IWebDriver driver, IWebElement elementToHover, IWebElement elementToClick)
        {
            Actions action = new Actions(driver);
            action.MoveToElement(elementToHover).Click(elementToClick).Build().Perform();
        }

       
        /// <summary>
        /// Explicit timeout
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="by"></param>
        /// <param name="timeoutInSeconds"></param>
        /// <returns></returns>
        public static IWebElement FindElement(IWebDriver driver, By by)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            var element = wait.Until(ExpectedConditions.ElementExists(by));
            return driver.FindElement(by);
        }

        public static IReadOnlyCollection<IWebElement> FindElements(IWebDriver driver, By by)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            var element = wait.Until(ExpectedConditions.ElementExists(by));
            return driver.FindElements(by);
        }
        /// <summary>
        /// Click the element
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="by"></param>

        public static void ClickElement(IWebDriver driver, By by, IWebElement elem)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            var element = wait.Until(ExpectedConditions.ElementToBeClickable(by));
            driver.FindElement(by).Click();
        }
        /// <summary>
        /// Verify Object Exist
        /// </summary>
        /// <param name="elementName"></param>
        /// <returns></returns>
        public static bool VerifyObjectExist(string elementName)
        {
            try
            {
                bool isElementDisplayed = _driver.FindElement(By.XPath(elementName)).Displayed;
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get Value by Key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="primaryCollection"></param>
        /// <param name="secondaryCollection"></param>
        /// <returns></returns>
        public static string GetValueByKey(string key, IReadOnlyCollection<IWebElement> primaryCollection, IReadOnlyCollection<IWebElement> secondaryCollection)
        {
            var index = 0;
            var value = string.Empty;
            foreach (var item in primaryCollection)
            {
                if (item.Text.Trim() == key)
                {
                    value = secondaryCollection.ToArray()[index].Text;
                    break;
                }
                index++;
            }
            return value;
        }
    }
}
