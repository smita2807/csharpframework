﻿using OpenQA.Selenium.Chrome;

namespace Assignments.Utilities
{
    public class Launch:Base
    {
        public void Browser(string browser)
        {
            switch (browser)
            {
                case "chrome":
                    _driver = new ChromeDriver();
                    break;
            }
        }
    }
}
