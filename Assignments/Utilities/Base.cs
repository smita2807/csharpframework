﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Configuration;


namespace Assignments.Utilities
{
    [SetUpFixture]
    public class Base
    {
        public static IWebDriver _driver;
        public ExtentReports extent;

        public ExtentTest test;

        [OneTimeSetUp]
        public void Setup()

        {

            var dir = @"~\\csharp\csharpframework\Assignments\Reports\";
           // var dir = TestContext.CurrentContext.TestDirectory+"\\Reports";
            var fileName = this.GetType().ToString() + ".html";
            var htmlReporter = new ExtentHtmlReporter(dir + fileName);

          //  htmlReporter.LoadConfig("extent-config.xml");

            extent = new ExtentReports();
            extent.AttachReporter(htmlReporter);
            

        }

        [SetUp]
        public void Beforetest()
        {

            test = extent.CreateTest(TestContext.CurrentContext.Test.Name);
            _driver = new ChromeDriver();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

           
            //Add UrL
            _driver.Url = "http://www.automation.com/";
            _driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void AfterTest()
        {

            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stacktrace = string.IsNullOrEmpty(TestContext.CurrentContext.Result.StackTrace)
                    ? ""
                    : string.Format("{0}", TestContext.CurrentContext.Result.StackTrace);
            Status logstatus;

            switch (status)
            {
                case TestStatus.Failed:
                    logstatus = Status.Fail;
                    break;
                case TestStatus.Inconclusive:
                    logstatus = Status.Warning;
                    break;
                case TestStatus.Skipped:
                    logstatus = Status.Skip;
                    break;
                default:
                    logstatus = Status.Pass;
                    break;
            }

            test.Log(logstatus, "Test ended with status " + logstatus + stacktrace);

            // log with snapshot
            test.Fail("details", MediaEntityBuilder.CreateScreenCaptureFromPath("screenshot.png").Build());

            // test with snapshot
            test.AddScreenCaptureFromPath("screenshot.png");
           //  extent.Flush();
            _driver.Quit();


        }

        [OneTimeTearDown]
        public void Teardown()
        {
            extent.Flush();
            extent.RemoveTest(test);       }



    }


}
