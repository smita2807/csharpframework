﻿using Assignments.Pages;
using Assignments.Utilities;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace Assignments
{
    [TestFixture]
    public class Assg01_RelativePathForObjectIdentification : Base
    {

        [Test]
        public void Test_Assg01_RelativePathForObjectIdentification()
        {
            HomePage homePage = new HomePage();
            // homePage.CloseLink.Click();
            Assert.AreEqual(homePage.LnkIndustries.Text, "Industries");
            IndustriesPage industriesPage = new IndustriesPage();
            CommonUtils.HoverAndClick(_driver, homePage.LnkIndustries, industriesPage.LnkSubMenuBuildingAutomation);

            BuildingAutomationPage buildingAutomationPage = new BuildingAutomationPage();
            Assert.AreEqual(buildingAutomationPage.LnkBuildAutoArchive.Text,"Building Automation e-Newsletter Archive");
            buildingAutomationPage.LnkBuildAutoArchive.Click();
             

                //Verify Navigation path
                List<string> expectedBreadCrumb = new List<string>
                    {
                        "Industries",
                        "Building Automation",
                        "Building Automation e-Newsletter Archive"
                    };

                IEnumerator<IWebElement> iter = buildingAutomationPage.BreadCrumbs.GetEnumerator();
                iter.MoveNext();
                string[] stringSeparators = new string[] { "\r\n" };
                string[] lines = iter.Current.Text.Split(stringSeparators, StringSplitOptions.None);
                foreach (var item in lines) { Assert.True(expectedBreadCrumb.Contains(item), "Breadcrum is not correct"); }
                string str = industriesPage.TitlesOfPapers.Text;
                Console.WriteLine("Titles are: ",str);
        }

    }
}

