﻿using Assignments.Pages;
using Assignments.Utilities;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;

namespace Assignments
{
    [TestFixture]
    public class Assg02_SearchKeyAndCategory : Base
    {
        [DatapointSource]
        private readonly string SearchKeyword = "Weidmuller";
        private readonly string CategoryCableSets = "Electrical Safety Ground Cluster / Cable Sets";

        [Test]
        
        public void Test_Assg02_SearchKeyAndCategory()
        {
            #region Declaration
            HomePage homePage = new HomePage();
            ProductsPage productsPage = new ProductsPage();
            #endregion

            try
            {
                //homePage.CloseLink.Click();
                CommonUtils.VerifyObjectExist(productsPage.TitleProductSearch.Text);
              
                Assert.IsTrue(productsPage.TitleProductSearch.Text.Contains("Product Search - Automation, Control & Instrumentation Products"));

                productsPage.TxtByKeyword.SendKeys(SearchKeyword);
                productsPage.BtnSearchNow.Click();
                
                foreach (var result in productsPage.SearchResults)
                {
                    Console.WriteLine(result.Text.Contains(SearchKeyword));
                }

                //Open Search Area
                productsPage.BtnOpenOrCloseSearch.Click();
                productsPage.ComboCategory.Click();
                productsPage.ComboCategory.SendKeys(CategoryCableSets);

                Actions builder = new Actions(_driver);
                builder.SendKeys(Keys.Enter);

                productsPage.BtnSearchNow.Click();

                foreach (var category in productsPage.SearchInfoCategory)
                {
                    Console.WriteLine(category.Text.Contains(CategoryCableSets));
                };

                productsPage.SearchProductLinks.Click();
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
           
        }

    }
}

