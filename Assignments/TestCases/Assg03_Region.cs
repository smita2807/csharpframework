﻿using Assignments.Pages;
using Assignments.Utilities;
using NUnit.Framework;
using System.Linq;

namespace Assignments
{
    [TestFixture]
    public class Assg03_Region : Base
    {
        [Test]
        public void Test_Assg03_Region()
        {
            #region Declaration
            HomePage homePage = new HomePage();
            SalarySurveyResultsPage salarySurveyResultsPage = new SalarySurveyResultsPage();
            #endregion

            CommonUtils.VerifyObjectExist(homePage.LnkJobCentre.Text);

            //Click on salary survey result 2015
            CommonUtils.HoverAndClick(_driver, homePage.LnkJobCentre, homePage.SubLnkSalarySuveyResults);
            Assert.IsTrue(salarySurveyResultsPage.LnkPastSalarySurveyResults.Displayed, "Past Salary Survey Results link is not present on the page.");

            //Click on past salary survey result link
            salarySurveyResultsPage.LnkPastSalarySurveyResults.Click();
            Assert.IsTrue(salarySurveyResultsPage.LnkSalarySurveyResults2015.Displayed, "Salary Survey Results 2015 link is not present on the page");

            //Click salary survey result 2015
            salarySurveyResultsPage.LnkSalarySurveyResults2015.Click();
            Assert.IsTrue(salarySurveyResultsPage.H1SalarySurveyResults2015.Displayed, "Salary Survey Results 2015 page Heading is not present");


            //Get the Average salary from table under Average salary by region of world for Region of world                
            var worldRegions = salarySurveyResultsPage.TblRowsRegionOfWorld;
            var worldAverageSalaries = salarySurveyResultsPage.TblAverageSalaryByRegionWorldRows;
            var worldPercentRespondents = salarySurveyResultsPage.TblRowsRegionWorldPercentRespondents;

            var avgSalaryValue = CommonUtils.GetValueByKey("United States", worldRegions, worldAverageSalaries);
            Assert.IsTrue(worldRegions.Any(x => x.Text.Trim() == "United States"), "Average Salary is present for United States in World Regions", worldRegions);

            //Get “Percent Respondents” from under “Average Salary by Region of the United States” section for Region of the United States
            var unitedStatesRegions = salarySurveyResultsPage.TblRowsRegionOfUS;
            var unitedStatesPercentRespondents = salarySurveyResultsPage.TblRowsRegionUSPercentRespondents;

            var percentValue = CommonUtils.GetValueByKey("West South Central (South)", unitedStatesRegions, unitedStatesPercentRespondents);
            Assert.IsTrue(unitedStatesPercentRespondents.Any(x => x.Text.Trim() == "21.3%"), "Percent Respondents value is present for West South Central (South) in Region of the United States", worldRegions);

        }

    }
}
